# Conway's Game Of Life

This is just yet another implementation of [Conway's Game Of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) written in Typescript.

It primarily started as a little excercise with [Javascript Generator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator) using a functional approach.

## Concepts

- `Cell` is a `[number, number]` tuple denoting coordinates on the _grid_.
- `Neighbors` is a function with parameter `cell: Cell` calculating the _neighbors_ of the given cell.
- `Generation` consists of all `cells: Cell[]` which are alive, and its `epoch: number`.
- `Generations` is a [Generator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator) yielding `Generation`s either for direct iteration with `for...of`, or for sequential consumption with its `next()` function. If you do not want to continue, i.e. do not care about any values, optionally pass `false` to `next()`.
- `World` is a factory function with parameters `cells: Cell[]` for the initial `Generation` and `epochMax?: number` which creates and returns the corresponding `Generations`.
- `Game` is a factory function with parameters `Neighbors` which creates and returns a `World`.

The defining parameter of a `Game` is the way how _neighbors_ for a `Cell` are calculated by means of a `Neighbors` function. Most importantly it indirectly determines the available coordinates which a `Cell` can occupy, i.e. if the _grid_ of the `Game` is `infinite`, `finite` (but unbounded) or `bounded`.

There are helper factory functions creating `World` functions with the some default `Neighbors` functions. The default `Neighbors` calculate `Neighbors` for a `Cell` in the literal sense, i.e. the `Cell[]` ((usually of length `8`)) directly adjacent to the given `Cell`:

- `bounded` with paramater `max: Cell` defining the maximum allowed `x` and `y` coordinates. The minimum `[0, 0]` is implicit. The game has hard (unconnected) bounds out ouf which no `Cell` can exist. This behavior is best conceptualized as _rectangle_ or _square_.

- `finite` with paramater `max: Cell` defining the maximum allowed `x` and `y` coordinates. The minimum `[0, 0]` is implicit. The game has connected bounds. A cell that would spawn outside any of these bounds (top, right, bottom, left) will spawn on the opposite side of the _grid_. This behavior is best conceptualized as a _torus_, (or) like a level in Pacman .

- `infinite` with no parameters. There are no boulds. Keep in mind that this especially means that a `Cell` can have negative coordinates. This behavior is best conceptualized as an _infinite plane_.

## Typescript

This package is build with Typescript and comes with its own type definitions.
Hense this project uses javascript generators only available since ES2015. So in order order to use within a Typescript based project make sure to set at least `"target": "es2015"` in your `tsconfig.json`.

```json
{
  "compilerOptions": {
    "target": "es2015"
    // ... and more
  }
}
```

## Basic usage example

```typescript
// Typescript example with "module": "commonjs" in tsconfig.json

import { games, Cell } from '@limpirium/game-of-life';
// const world = games.bounded([10, 10]); // or
// const world = games.finite([10, 10]); // or
const world = games.infinite();
const glider: Cell[] = [
  [2, 1],
  [3, 2],
  [1, 3],
  [2, 3],
  [3, 3],
];

// direct iteration
let generations = world(
  glider,
  1000, // set a maximum of 1000 epochs or iteration will probably run forever
);
for (let generation of generations) {
  console.log(generation);
}

// manual, sequential access
generations = world(glider);
console.log(generations.next()); // -> {value: {cells: [...], epoch: 0}, done: false}
console.log(generations.next()); // -> {value: {cells: [...], epoch: 1}, done: false}
console.log(generations.next()); // -> {value: {cells: [...], epoch: 2}, done: false}
// to stop pass `false` to `next()`
console.log(generations.next(false)); // -> {value: {cells: [...], epoch: 3}, done: true}
console.log(generations.next()); // -> {value: undefined, done: true}
```

## Todos / next steps

- check in browser
- write some tests
- stop generation if there are no changes from one generation to the other
- provide a library of some famous initial `Cell[]`

## Ideas

- possibility for custom birth/death conditions
- custom `Neighbors` functions and explore how this effects the Game Of Life. Haven't tried it or checked for research myself yet, but may be interesting 🤔 or maybe not 🤷
