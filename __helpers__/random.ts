import { Cell } from '../src/Cell/types';

const sign = () => Math.round(Math.random()) * 2 - 1;
const positiv = (): number => Math.ceil(Math.random() * 100); // never 0
const negative = (): number => -1 * positiv();
const coordinate = (): number => sign() * positiv();
const cell = (): Cell => [coordinate(), coordinate()];
const cells = (): Cell[] => {
  let count = 10 + Math.floor(Math.random() * 10);
  return [
    ...(function* () {
      while (count >= 0) {
        yield cell();
        count--;
      }
    })(),
  ].filter(
    ([ax, ay], index, all) =>
      index === all.findIndex(([bx, by]) => ax === bx && ay === by),
  );
};

const index = (cells: Cell[]): number =>
  Math.floor(Math.random() * cells.length);

export default {
  positiv,
  negative,
  coordinate,
  cell,
  cells,
  index,
};
