import { Cell, sort, unique } from '../Cell';
import { Neighbors } from '../Neighbors';
import rule from './rule';

export const evolve = (cells: Cell[], neighbors: Neighbors): Cell[] => {
  return sort(
    unique([...cells, ...cells.flatMap(neighbors)]).filter(
      rule(cells, neighbors),
    ),
  );
};

export default evolve;
