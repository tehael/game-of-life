import { Cell, Discriminator, includes } from '../Cell';
import { Neighbors } from '../Neighbors';

const rule = (cells: Cell[], neighbors: Neighbors): Discriminator => {
  const alive = includes(cells);
  return (cell: Cell) => {
    const count = neighbors(cell).filter(alive).length;
    return count === 3 || (count === 2 && alive(cell));
  };
};

export default rule;
