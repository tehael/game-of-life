import { Cell } from '../Cell';

export type Generation = {
  epoch: number;
  cells: Cell[];
};
export type Generations = Generator<
  Generation,
  Generation,
  boolean | undefined
>;
export type World = (cells: Cell[], epochMax?: number) => Generations;
