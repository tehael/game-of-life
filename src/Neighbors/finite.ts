import { Cell } from '../Cell';
import { Neighbors } from './types';

const finite = ([xMax, yMax]: Cell): Neighbors => ([x, y]: Cell) => {
  const top = y > 0 ? y - 1 : yMax;
  const bottom = y < yMax ? y + 1 : 0;
  const left = x > 0 ? x - 1 : xMax;
  const right = x < xMax ? x + 1 : 0;
  return [
    [left, top],
    [x, top],
    [right, top],
    [left, y],
    [right, y],
    [left, bottom],
    [x, bottom],
    [right, bottom],
  ] as Cell[];
};

export default finite;
