import { Cell } from '../Cell';
import { Neighbors } from './types';

const infinite = (): Neighbors => ([x, y]: Cell) =>
  [
    [x - 1, y - 1], // left-top
    [x, y - 1], // top
    [x + 1, y - 1], // right-top
    [x - 1, y], // left
    [x + 1, y], // right
    [x - 1, y + 1], // left-bottom
    [x, y + 1], // bottom
    [x + 1, y + 1], // right-bottom
  ] as Cell[];

export default infinite;
