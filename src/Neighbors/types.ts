import { Cell } from '../Cell';

export type Neighbors = (cell: Cell) => Cell[];
