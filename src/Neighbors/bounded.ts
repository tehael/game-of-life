import { Cell } from '../Cell';
import { Neighbors } from './types';

const bounded = ([xMax, yMax]: Cell): Neighbors => ([x, y]: Cell) =>
  [
    ...(function* () {
      if (x > 0 && y > 0) yield [x - 1, y - 1]; // left-top
      if (y > 0) yield [x, y - 1]; // top
      if (x < xMax && y > 0) yield [x + 1, y - 1]; // right-top
      if (x > 0) yield [x - 1, y]; // left
      if (x < xMax) yield [x + 1, y]; // right
      if (x > 0 && y < yMax) yield [x - 1, y + 1]; // left-bottom
      if (y < yMax) yield [x, y + 1]; // bottom
      if (x < xMax && y < yMax) yield [x + 1, y + 1]; // right-bottom
    })(),
  ] as Cell[];

export default bounded;
