import { Cell, sort, unique } from '../Cell';
import { Neighbors } from '../Neighbors';
import { Generation } from '../World';
import { Game } from './types';
import tick from './tick';

const game: Game = (neighbors: Neighbors) =>
  function* generator(cells: Cell[], epochMax?: number) {
    const next = tick(neighbors);
    let generation: Generation = {
      epoch: 0,
      cells: sort(unique(cells)),
    };
    let more = (yield generation) ?? true;
    while (
      more &&
      generation.cells.length > 0 &&
      (!epochMax || generation.epoch < epochMax)
    ) {
      generation = next(generation);
      more = (yield generation) ?? true;
    }
    return next(generation);
  };

export default game;
