import { Cell } from '../Cell';
import { bounded, finite, infinite } from '../Neighbors';
import { World } from '../World';
import game from './game';

const games = {
  bounded: (max: Cell): World => game(bounded(max)),
  finite: (max: Cell): World => game(finite(max)),
  infinite: (): World => game(infinite()),
};

export default games;
