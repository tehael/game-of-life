import { Neighbors } from '../Neighbors';
import { World } from '../World';

export type Game = (neighbors: Neighbors) => World;
