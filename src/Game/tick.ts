import { Neighbors } from '../Neighbors';
import { Generation, evolve } from '../World';

const tick = (neighbors: Neighbors) => ({
  epoch,
  cells,
}: Generation): Generation =>
  ({
    epoch: epoch + 1,
    cells: evolve(cells, neighbors),
  } as Generation);

export default tick;
