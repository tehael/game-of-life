import * as exported from '.';
import includes from './includes';
import sort from './sort';
import unique from './unique';

describe('module', () => {
  it('exports public API', () => {
    expect(exported.includes).toEqual(includes);
    expect(exported.sort).toEqual(sort);
    expect(exported.unique).toEqual(unique);
  });
});
