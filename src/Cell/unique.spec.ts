import random from '../../__helpers__/random';
import { Cell } from './types';
import same from './same';
import unique from './unique';

describe('unique', () => {
  it('returns Cell[] without duplicates', () => {
    const cells = random.cells();
    let index = random.index(cells);
    const cell = [...cells[index]] as Cell; // make sure it's a new Cell reference with the same coordinates
    const before = [...cells];
    index = random.index(cells);
    before.splice(index, 0, cell);
    expect(before.filter(same(cell)).length).toBe(2); // just to be sure we really have a duplicate
    const after = unique(before);
    expect(after.length).toBeLessThanOrEqual(cells.length);
    expect(after.length).toBeLessThan(before.length);
    expect(after.filter(same(cell)).length).toBe(1);
  });
});
