import random from '../../__helpers__/random';
import * as equal from './equal';
import same from './same';
let spy: jest.SpyInstance;
describe('same', () => {
  it('returns a predicate for Cell testing equality with that cell', () => {
    spy = jest.spyOn(equal, 'default');
    const cell = random.cell();
    const other = random.cell();
    same(cell)(other);
    expect(spy).toBeCalledWith(cell, other);
    spy.mockRestore();
  });
});
