import { Cell } from './types';
import equal from './equal';

export const same = (cell: Cell) => (other: Cell): boolean =>
  equal(cell, other);

export default same;
