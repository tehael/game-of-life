import { Cell } from './types';

const equal = ([ax, ay]: Cell, [bx, by]: Cell): boolean =>
  ax === bx && ay === by;

export default equal;
