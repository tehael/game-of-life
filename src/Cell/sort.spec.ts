import random from '../../__helpers__/random';
import sort from './sort';

describe('sort', () => {
  it('returns a sorted Cell[]', () => {
    const cells = random.cells();
    const index = random.index(cells);
    cells.push([random.coordinate(), cells[index][1]]);
    sort(cells).forEach((cell, index, all) => {
      if (index < all.length - 1) {
        const next = all[index + 1];
        expect(cell[0] <= next[0] || cell[1] <= next[1]).toBe(true);
      }
    });
  });
});
