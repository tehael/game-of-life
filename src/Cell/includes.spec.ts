import random from '../../__helpers__/random';
import { Cell } from './types';
import includes from './includes';

describe('isIn', () => {
  it('returns a predicate for Cell[] which returns true if cell is in the given Cell[]', () => {
    const cells = random.cells();
    const index = random.index(cells);
    const cell = [...cells[index]] as Cell; // make sure it's a new Cell reference with the same coordinates
    expect(includes(cells)(cell)).toBe(true);
  });

  it('returns a predicate for Cell[] which returns false if cell is NOT in the given Cell[]', () => {
    const cells = random.cells();
    const index = random.index(cells);
    const cell = [...cells[index]] as Cell; // make sure it's a new Cell reference with the same coordinates
    cells.splice(index, 1); // remove the original Cell
    expect(includes(cells)(cell)).toBe(false);
  });
});
