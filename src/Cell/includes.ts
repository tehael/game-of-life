import { Cell } from './types';
import same from './same';

const includes = (cells: Cell[]): ((cell: Cell) => boolean) => (cell: Cell) =>
  cells.some(same(cell));

export default includes;
