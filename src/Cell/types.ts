export type Cell = [number, number];
export type Discriminator = (cell: Cell) => boolean;
