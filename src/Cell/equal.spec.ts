import random from '../../__helpers__/random';
import equal from './equal';

describe('equal', () => {
  describe('eq', () => {
    it('returns true if 2 cells have the same coordinates', () => {
      const cell = random.cell();
      expect(equal(cell, [...cell])).toBe(true);
    });
    it('returns false if 2 cells differ in at least one coordinate', () => {
      const cell = random.cell();
      expect(equal(cell, [cell[0] + random.coordinate(), cell[1]])).toBe(false);
      expect(equal(cell, [cell[0], cell[1] + random.coordinate()])).toBe(false);
      expect(
        equal(cell, [
          cell[0] + random.coordinate(),
          cell[1] + random.coordinate(),
        ]),
      ).toBe(false);
    });
  });
});
