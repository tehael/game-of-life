import { Cell } from './types';

const compare = ([ax, ay]: Cell, [bx, by]: Cell): number => {
  if (ay === by) return ax - bx;
  return ay - by;
};

const sort = (cells: Cell[]): Cell[] => [...cells].sort(compare); // keeping it pure

export default sort;
