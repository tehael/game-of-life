import { Cell } from './types';
import same from './same';

const unique = (cells: Cell[]): Cell[] =>
  cells.filter((cell, index, all) => index === all.findIndex(same(cell)));

export default unique;
